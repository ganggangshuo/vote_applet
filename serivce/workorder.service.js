import { HttpService } from '../core/service/core.service'

export class WorkOrderHttpService extends HttpService {
  constructor() {
    super("WorkOrder/Index");
  }

  /**
   * 获取所有分类
   */
  GetStatisticsByCatalogue() {
    return this.httpGet(this.resolveUrl("GetStatisticsByCatalogue"));
  }

  /**
   *  获取我的待评价
   */
  GetByUser(data) {
    return this.httpGet(this.resolveUrl("GetByUser"), this.resolveRequestParams(data));
  }

  /**
   * 获取工单记录
   */
  getServiceOrderLog(id) {
    return this.httpGet(this.resolveUrl("GetServiceOrderLog/" + id));
  }

}


export class WorkCatalogueHttpService extends HttpService {
  constructor() {
    super("WorkOrder/Catalogue");
  }

  GetByCode(code) {
    return this.httpGet(this.resolveUrl("GetByCode/" + code));
  }

}

export class WorkQuestionService extends HttpService {
  constructor() {
    super("WorkOrder/Question")
  }
}

export class ShCrlandWorkOrderService extends HttpService {
  constructor() {
    super("ShCrlandWorkOrder/Index");
  }

  withdraw(data) {
    return this.httpPut(this.resolveUrl("Withdraw"), data);
  }


  GetByUser(data) {
    return this.httpGet(this.resolveUrl("GetByUserExt"), this.resolveRequestParams(data));
  }

  getPagedExt(data) {
    return this.httpGet(this.resolveUrl("GetExt"), this.resolveRequestParams(data));
  }

  repairFeeAssess(id,fee){
    return this.httpPost(this.resolveUrl("RepairFeeAssess", id), { fee: fee});
  }

  complete(id, problem) {
    return this.httpGet(this.resolveUrl("RepairOperation", id) + '?RepairStatus=16&problem=' + problem);
  }

  dispatch(id, problem) {
    return this.httpGet(this.resolveUrl("RepairOperation", id) + '?RepairStatus=30&problem=' + problem);
  }


  forwarding(data) {
    return this.httpPut('ForwardingExt', data);
  }

  isPropertyCharge() {
    return this.httpGet('IsPropertyCharge');
  }
}

export const shcrlandworkorderService = new ShCrlandWorkOrderService();
export const workOrderService = new WorkOrderHttpService();
export const workCatalogService = new WorkCatalogueHttpService();
export const workQuestionService = new WorkQuestionService();
