import { HttpService } from '../core/service/core.service'

class VoteService extends HttpService {
  constructor() {
    super('Vote/Index');
  }
  /**
* 查询列表
* 
* @returns 
* @memberof VoteService
*/
  GetVoteList() {
    return this.httpGet(this.resolveUrl("Get"));
  }
  /**
* 查询详情
* 
*@param id:Number 数据ID
* @returns 
* @memberof VoteService
*/
  GetVoteInfo(id) {
    return this.httpGet(this.resolveUrl("Get/" + id));
  }
 /**
* 修改状态
* 
*@param id:Number 数据ID
*@param body:json 修改内容
* @returns 
* @memberof VoteService
*/
  Modify(id,body) {
    return this.httpPut("PutIsValid/" + id,body);
  }

  
    /**
* 查询排行
* 
*@param id:Number 数据ID
* @returns 
* @memberof VoteService
*/
  GetVoteRanking() {
    return this.httpGet(this.resolveUrl("GetVoteRanking"));
  }
}
export const voteService = new VoteService();


/*****参与投票*******/

class VoteOptionJoinService extends HttpService {
  constructor() {
    super('VoteOptionJoin/VoteOptionJoin');
  }
  /**
* 参与投票
* 
* @returns 
* @memberof VoteService
*/
  VoteJoin(vmdata) {
    return this.httpPost(this.resolveUrl("VoteJoin/"),vmdata);
  }
}
export const voteOptionJoinService = new VoteOptionJoinService();

