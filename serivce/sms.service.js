import { HttpService } from '../core/service/core.service'

import { auth, server } from "../reco.config";

export class SMSHttpService extends HttpService {
  constructor() {
    super("SMS/Index");
  }

  sendVerifyCode(mobile) {
    return this.httpGet("SendVerifyCode", this.resolveRequestParams({ mobile }));
  }

  regist(data) {
    return this.httpPost("Regist", data);
  }

  sendLoginCode(mobile) {
    return this.httpGet("SendLoginCode", this.resolveRequestParams({ mobile }));
  }

  login(data) {
    return clearToken(), this.httpPost("Login", { ApiKey: server.ApiKey.ApiKey, ApiSecret: server.ApiKey.Secret, ...data });
  }

  sendResetCode(mobile) {
    return this.httpGet("SendResetCode", this.resolveRequestParams({ mobile }));
  }

  restPassword(data) {
    return this.httpPost("RestPassword", data);
  }

  sendBindCode(mobile) {
    return this.httpGet("SendBindCode", this.resolveRequestParams({ mobile }));
  }

  bindMobile(data) {
    return this.httpPost("BindMobile", data);
  }
}

export const smsService = new SMSHttpService();
