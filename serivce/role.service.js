import { HttpService } from '../core/service/core.service'

class RoleHttpService extends HttpService {
  constructor() {
    super("Role/Index");
  }

  GetAllGroupUser(data) {
    return this.httpGet(this.resolveUrl("GetAllGroupUser?id=" + data));
  }
}

export const roleService = new RoleHttpService();
