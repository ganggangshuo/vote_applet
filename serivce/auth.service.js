import { HttpService } from '../core/service/core.service'

class AuthHttpService extends HttpService {
  constructor() {
    super("Authorization/Index");
  }
  getCurrentUser() {
    return this.currentUser ? promise_1.transform(Promise.resolve(this.currentUser)) : this.httpGet("GetCurrentUser").success(d => (this.currentUser = d));
  }
  clearCurrentUser() {
    delete this.currentUser;
  }
  getEdit(data) {
    return this.httpGet(this.resolveUrl("GetEdit/" + data.id), data);
  }
  modifyPersonMessage(data, userId) {
    return this.httpPut(this.resolveUrl("PutAccount/" + userId), data);
  }
}

export const authService = new AuthHttpService();
