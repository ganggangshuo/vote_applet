import { HttpService } from '../core/service/core.service'

class AuthorizationHttpService extends HttpService {
  constructor() {
    super("Authorization/Index");
  }
  GetEdit(data) {
    return this.httpGet(this.resolveUrl("GetEdit?id=" + data));
  }
}

export const authorizationService = new AuthorizationHttpService();