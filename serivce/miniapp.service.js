import { HttpService } from '../core/service/core.service'
import { server } from "../reco.config";

class MiniAppHttpService extends HttpService {
  constructor() {
    //super("Authorization/Login");
    super("MiniApp/Index");

  }

  authUserInfo(code, data) {
    var obj = {
      "ApiKey": "Bitech\\PC",
      "Secret": "44678314ba0efa0c",
      "LoginName": "demo",
      "Password": "123456"
    }

    //return this.httpPost('Login/', obj );

    return this.httpPost(this.resolveUrl(`authUserInfo?code=${code}&apiKey=${server.ApiKey.ApiKey}&apiSecret=${server.ApiKey.Secret}`), data);
  }
  bindUser(data) {
    return this.httpPost(this.resolveUrl(`bindUser?realname=${data.RealName}&openID=${data.openID}&apiKey=${server.ApiKey.ApiKey}&apiSecret=${server.ApiKey.Secret}`));
  }

  login(data) {
    var obj = {
      "apiKey": server.ApiKey.ApiKey,
      "apiSecret": server.ApiKey.Secret,
      "openID": data,
    }
    return this.httpPost(this.resolveUrl('MiniLogin?apiKey=' + server.ApiKey.ApiKey + '&apiSecret=' + server.ApiKey.Secret + '&openID=' + data + ''));
  }


}

export const miniappService = new MiniAppHttpService();

