import { HttpService } from '../core/service/core.service'

export class PictureHttpService extends HttpService {
  constructor(controllerName, http) {
    super(controllerName || "File/Picture", http);
  }

  getPictureUrls(data) {
    return this.httpGet(this.resolveUrl("GetPictureUrls"), this.resolveRequestParams(data));
  }

  getPictureUrl(data) {
    return this.httpGet(this.resolveUrl("GetPictureUrl"), this.resolveRequestParams(data));
  }

  getPictureSrc(data) {
    return this.getRootUrl(this.transformUrl("Get?" + this.http.urlEncodeParams(data).toString()));
  }

  getEditorUploadUrl() {
    return this.getRootUrl(this.transformUrl("UploadIMG") + `?${this.getUrlToken()}`);
  }
}

export const pictureService = new PictureHttpService();
