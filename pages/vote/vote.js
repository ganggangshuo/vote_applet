// pages/vote/vote.js
import { voteService, voteOptionJoinService } from '../../serivce/vote.service';


Page({

  /**
   * 页面的初始数据
   */
  data: {
    vid: "0",
    info: {},
    classbool: 0,
    HasMiniAdmin: false,
    IsJoin: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    // if (!wx.getStorageSync('Token'))
    //   wx.navigateTo({
    //     url: '../binding/binding'
    //   })

    _this.setData({ vid: options.vid });
    voteService.GetVoteInfo(_this.data.vid).success((x) => {
      _this.setData({ info: x, classbool: x.IsValid ? 1 : 0, IsJoin: x.IsJoin > 0 ? x.IsJoin : 0 })
    }
    ).error((res) => {
      console.log(res);
      wx.showModal({
        title: "错误消息",
        content: res.errMsg,
      })
    });
    console.log(_this.data)
  },
  /**
*投票 
*/
  votetab: function (event) {
    var _UID = wx.getStorageSync("UID");
    var _VoteID = event.currentTarget.dataset.voteid;
    var _isvalid = event.currentTarget.dataset.isvalid;
var _this=this;
    var _VoteOptionID = event.currentTarget.id;
    if (_isvalid != true) {
      wx.showToast({
        title: "未发布",
        image: '../../images/error.png',
        duration: 2000
      })
    }
    else {
      voteOptionJoinService.VoteJoin({ InputerID: _UID, VoteID: _VoteID, VoteOptionID: _VoteOptionID }).success((res) => {
        if (res.state) {
          _this.setData({});
          // _VoteOptionID
          
          _this.setData({ IsJoin: _VoteOptionID});
          wx.showToast({
            title: res.msg,
            icon: 'success',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: res.msg,
            image: '../../images/error.png',
            duration: 2000
          })
        }
      });
    }
  },
  /**
   *管理员投票开关 
   **/
  openIsValid: function (event) {
    var _this = this;
    var _IsValid = false;
    if (_this.data.classbool == "1") {
      _this.setData({ classbool: 0 });
      _IsValid = false;
    }
    else {
      _this.setData({ classbool: 1 });
      _IsValid = true;
    }

    voteService.Modify(_this.data.vid, { IsValid: _IsValid }).success((res) => {

    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // }
})