// pages/award/award.js
import { voteService } from '../../serivce/vote.service';

Page({
  toast2: function (event) {
    console.log(event)
    // wx.navigateTo({
    //   url: '../onepice/onepice?vid=' + event.currentTarget.id + '&likecount=' + event.currentTarget.dataset.likecount + '&dislikecount=' + event.currentTarget.dataset.dislikecount
    // })
  },
  toast3: function (event) {
    // wx.navigateTo({
    //   url: '../secondpice/secondpice?vid=' + event.currentTarget.id + '&likecount=' + event.currentTarget.dataset.likecount + '&dislikecount=' + event.currentTarget.dataset.dislikecount
    // })
  },
  toast4: function (event) {
    // wx.navigateTo({
    //   url: '../thirdpice/thirdpice?vid=' + event.currentTarget.id + '&likecount=' + event.currentTarget.dataset.likecount + '&dislikecount=' + event.currentTarget.dataset.dislikecount
    // })
  },
  /**
   * 页面的初始数据
   */
  data: {
    info: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    // if (!wx.getStorageSync('Token'))
    //   wx.navigateTo({
    //     url: '../binding/binding'
    //   })
    voteService.GetVoteRanking().success(function (res) {
      console.log(res);
      _this.setData({ info: res });
    }).error((res) => {
      console.log(res);
      wx.showModal({
        title: "错误消息",
        content: res.errMsg,
      })
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var _this = this;
    _this.setData({ info:null})
    voteService.GetVoteRanking().success(function (res) {
      console.log(res);
      _this.setData({ info: res });
      setTimeout(function(){
      wx.stopPullDownRefresh();
      },500)
    }).error((res) => {
      console.log(res);
      wx.showModal({
        title: "错误消息",
        content: res.errMsg,
      })
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // }
})