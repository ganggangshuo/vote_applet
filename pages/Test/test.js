// pages/Test/test.js
import { authService } from '../../serivce/auth.service';
import { shcrlandworkorderService } from '../../serivce/workorder.service.js';
import { format, resolveUrl } from '../../utils/util';
import { pictureService } from '../../serivce/picture.service';
import { authorizationService } from '../../serivce/authorization.services';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userdata: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 121.611441, 31.184195
    var lat = 31.184;
    var long = 121.611;
    var minlat = 31.151;
    var maxlat = 31.251;
    var minlong = 121.475;
    var maxlong = 121.675;
    var _this = this;
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        console.log(res)
        var latitude = res.latitude;
        var longitude = res.longitude;
        if (latitude > 0 && longitude > 0) {
          if (latitude >= minlat && latitude <= maxlat) {
            console.log("在经度范围内")
            if (longitude >= minlong && longitude <= maxlong) {
              console.log("在纬度范围内")

            }
            else {
              wx.showModal({
                title: "错误消息",
                content: "您不在规定范围内"
              })
            }
          }
          else {
            wx.showModal({
              title: "错误消息",
              content: "您不在规定范围内"
            })
          }
        }
      }
    })
    // var _this = this;
    // console.log(authorizationService)
    // authorizationService.GetEdit('1094560285458461').success(function (userdata) {
    //   _this.setData({
    //     userdata: userdata
    //   })
    //   console.log(_this.data.array);
    // })
  },
/**
   * 下拉刷新
   */
  onPullDownRefresh: function () {
    wx.getLocation({
      type: 'wgs84', //返回可以用于wx.openLocation的经纬度
      success: function (res) {
        console.log(res)
        var latitude = res.latitude
        var longitude = res.longitude
        wx.openLocation({
          latitude: latitude,
          longitude: longitude,
          scale: 28
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {
  
  // }
})