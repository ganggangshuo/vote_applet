// pages/onepice/onepice.js
import { voteService } from '../../serivce/vote.service';

Page({ 
  /**
     * 页面的初始数据
     */
  data: {
    info: [],
    likecount: 0,
    dislikecount: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;

    _this.setData({
      likecount: options.likecount||0,
      dislikecount: options.likecount||0
    });

    voteService.GetVoteInfo(options.vid).success(function (res) {
      _this.setData({ info: res });
    });
  },
  // onPullDownRefresh: function () {
  //   wx.redirectTo({
  //     url: '../secondpice/secondpice',
  //   })
  // }
})
