// pages/secondpice/secondpice.js
import { voteService } from '../../serivce/vote.service';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: [],
    likecount: 0,
    dislikecount: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    _this.setData({
      likecount: options.likecount || 0,
      dislikecount: options.likecount || 0
    });

    voteService.GetVoteInfo(options.vid).success(function (res) {
      _this.setData({ info: res });
    });
  },
  // /**
  //  * 下拉刷新
  //  */
  // onPullDownRefresh: function () {
  //   wx.redirectTo({
  //     url: '../onepice/onepice',
  //   })
  // },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {
  
  // }
})