// pages/index/inex.js
import { refreshToken } from '../../core/func/auth';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo: {},
    hasLocation: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    var lat = 31.184;
    var long = 121.611;
    var minlat = 31.151;
    var maxlat = 31.251;
    var minlong = 121.475;
    var maxlong = 121.675;
    var _this = this;
    wx.showLoading({
      title: '定位中,请稍后...',
    })
    wx.getLocation({
      type: 'wgs84',
      fail: function (res) {
        console.log(res)
        wx.hideLoading();
        wx.showModal({
          title: "错误消息",
          content: "未获取到位置信息：" + res.errMsg
        })
      },
      success: function (res) {
        wx.hideLoading();
        console.log(res)
        var latitude = res.latitude;
        var longitude = res.longitude;
        if (latitude > 0 && longitude > 0) {
          if (latitude >= minlat && latitude <= maxlat) {
            console.log("在经度范围内")
            if (longitude >= minlong && longitude <= maxlong) {
              console.log("在纬度范围内")
              _this.setData({ hasLocation: true })
              if (!wx.getStorageSync('Token'))
                wx.navigateTo({
                  url: '../binding/binding'
                })
              var _Avatar = wx.getStorageSync("Avatar") || wx.getStorageSync("avatarUrl") || "../../images/user.png";
              var _RealName = wx.getStorageSync("RealName") || wx.getStorageSync("nickName") || "";

              _this.setData({ userinfo: { Avatar: _Avatar, RealName: _RealName } });
            }
            else {
              wx.showModal({
                title: "错误消息",
                content: "您不在规定范围内"
              })
            }
          }
          else {
            wx.showModal({
              title: "错误消息",
              content: "您不在规定范围内"
            })
          }
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var _this = this;
    var _Avatar = wx.getStorageSync("Avatar") || wx.getStorageSync("avatarUrl") || "../../images/user.png";
    var _RealName = wx.getStorageSync("RealName") || wx.getStorageSync("nickName") || "";
    _this.setData({ userinfo: { Avatar: _Avatar, RealName: _RealName } });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // },
  toast2: function () {
    wx.navigateTo({
      url: '../onepice/onepice'
    })
  },
  toast3: function () {
    wx.navigateTo({
      url: '../secondpice/secondpice'
    })
  },
  toast4: function () {
    wx.navigateTo({
      url: '../thirdpice/thirdpice'
    })
  },
  toast5: function () {
    // wx.showModal({
    //   title: '暂未公布投票结果',
    //   content: '暂未公布投票结果',
    // })
    // if (false) {
      wx.navigateTo({
        url: '../award/award'
      })
    // }
  },
  toast6: function () {
    wx.navigateTo({
      url: '../pice/pice'
    })
  },
  toast7: function () {
    wx.navigateTo({
      url: '../program/program_list'
    })
  },
  toast8: function () {
    wx.navigateTo({
      url: '../flow/flow'
    })
  }
})