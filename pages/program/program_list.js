// pages/program/program_list.js
// import { authService } from '../../serivce/auth.service';
// import { format, resolveUrl } from '../../utils/util';
// import { pictureService } from '../../serivce/picture.service';
import { voteService } from '../../serivce/vote.service';

Page({

  //页面跳转
  toast1: function (event) {
    wx.navigateTo({
      url: '../vote/vote?vid=' + event.currentTarget.id
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    votedatalist: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var _this = this;
    // if (!wx.getStorageSync('Token'))
    //   wx.navigateTo({
    //     url: '../binding/binding'
    //   })
    //查询列表数据
    voteService.GetVoteList().success(function (res) {
      _this.setData({
        votedatalist: res
      })
    }).error((res) => {
      console.log(res);
      wx.showModal({
        title: "错误消息",
        content: res.errMsg,
      })
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var _this=this;
    //查询列表数据
    voteService.GetVoteList().success(function (res) {
      _this.setData({
        votedatalist: res
      })
    }).error((res) => {
      console.log(res);
      wx.showModal({
        title: "错误消息",
        content: res.errMsg,
      })
    });

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // }
})