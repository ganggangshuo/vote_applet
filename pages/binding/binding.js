// pages/binding/binding.js
import { miniappService, authorizationHttpService } from '../../serivce/miniapp.service';
import { isCellphone } from '../../utils/util';
import { encode, decode } from '../../utils/base64';
import { refreshToken } from '../../core/func/auth';



//获取应用实例
var app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    nickName: '',
    avatarUrl: '',
    delay: 0,
    mobile: '',
    code: '',
    userInfo: {},
    openID: '',
    isbinding: false,
    isshouquan: 0
  },
  getUserInfo: function (_this) {
    var userInfo, name, url, gender, province, city, country;
    wx.getUserInfo({
      fail: function (e) {
        console.log(e);
        wx.showModal({
          title: "错误消息",
          content: "未获取用户授权:" + e.errMsg,
        })
        _this.setData({ isshouquan: -1 });
      },
      success: function (res) {
        _this.setData({ isshouquan: 1 });
        console.log(res);
        userInfo = res.userInfo;
        userInfo.headimgurl = userInfo.avatarUrl;
        userInfo.sex = userInfo.gender;

        wx.setStorageSync("avatarUrl", userInfo.avatarUrl);
        wx.setStorageSync("nickName", userInfo.nickName);

        _this.setData({
          nickName: userInfo.nickName,
          avatarUrl: userInfo.avatarUrl
        })

        wx.login({
          success: function (d) {

            wx.showLoading({
              title: '加载中,请稍后...',
            })
            //登陆
            miniappService.authUserInfo(d.code, userInfo).success(function (u) {
              console.log(u)
              wx.setStorageSync("HasMiniAdmin", u.HasMiniAdmin);

              // if (u.Token == "" || (!wx.getStorageSync('RealName') || wx.getStorageSync('RealName') == "")) {
              //   _this.setData({ isbinding: true });
              //   wx.hideLoading();
              // }
              // else {
              //   _this.setData({ isbinding: false });
              //   u.Token && _this.onLogin(u.Token, u.Account);
              // }
              miniappService.login(u.Account.LoginName).success(function (lu) {
                wx.hideLoading();
                if (lu.Token != "") {
                  wx.hideLoading();
                  _this.setData({ openID: u.Wechat.openid });
                   _this.onLogin(lu.Token, u.Account);
                }
              });
            }).error((res) => {
              console.log(res);
              wx.showModal({
                title: "错误消息",
                content: res.Message,
              })
            })
          },
          fail: function (e) {
            console.log(e);
            wx.showModal({
              title: "错误消息",
              content: e.errMsg,
            })
          }
        })

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    _this.getUserInfo(_this);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // },
  onchange: function (e) {
    this.setData({ [e.target.id]: e.detail.value })
  },
  /*
  重新获取授权
   */
  bindgetuserinfo: function () {
    var _this = this;
    _this.getUserInfo(_this);
  },
  /**
   * 登陆
   */
  onLogin: function (token, account) {
    refreshToken(false, token)
    if (account != undefined) {
      wx.setStorageSync("UID", account.ID);
      wx.setStorageSync("RealName", account.RealName);
      wx.setStorageSync("Avatar", account.Avatar);
    }
    wx.navigateBack({
      url: '../index/index'
    })
  },
  /**
   * 绑定个人登陆
   */
  bindsubmit: function () {

    var RealName = this.data.RealName, code = this.data.code, _this = this;

    if (!RealName) {
      wx.showToast({
        title: '姓名不能为空',
        image: '../../images/error.png'
      })

      return;
    }
    wx.showLoading({
      title: '加载中,请稍后...',
    })
    miniappService.login({ code: code, openID: this.data.openID, RealName: RealName }).success(function (d) {
      // _this.onLogin(d);
      if (u.Token != "") {
        d.Token && _this.onLogin(d.Token, d.Account);
        wx.hideLoading();
      }
    }).error(function (d) {
      wx.hideLoading();
      wx.showToast({
        title: d.Message,
        image: '../../images/error.png'
      })
    })

  }
})