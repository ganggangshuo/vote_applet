Page({
  data: {
    imgUrls: [
      '../../images/piceimg1.png',
      '../../images/piceimg2.png',
      '../../images/piceimg3.png'
    ],
    indicatorDots: true,
    autoplay: false,
    interval: 5000,
    duration: 500,
    circular:true
  }
})