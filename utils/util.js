import { server } from "../reco.config";

const CELLPHONE_REGEXP = /^1[3|4|5|7|8][0-9]{9}$/;
const EMAIL_REGEXP = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;

export function isCellphone(value) {
  return value && CELLPHONE_REGEXP.test(value);
}

export function isEmail(value) {
  return value && EMAIL_REGEXP.test(value);
}

export function format(date, fmt) {
  if (!fmt)fmt='yyyy-MM-dd';

  date = getDate(date);

  const o = {
    "M+": date.getMonth() + 1, // 月份
    "d+": date.getDate(), // 日
    "h+": date.getHours(), // 小时
    "m+": date.getMinutes(), // 分
    "s+": date.getSeconds(), // 秒
    "q+": Math.floor((date.getMonth() + 3) / 3), // 季度
    S: date.getMilliseconds() // 毫秒
  };

  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));

  for (const k in o) if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));

  return fmt;
}

export function getDate(date) {
  return date instanceof Date ? date : new Date(date.replace(/\-/g, "/").replace(/T/g, " "));
}


function transform(url) {
  return url && url.charAt(0) === "~" ? ((url = url.substr(2)), `${server.url}${url}`) : url;
}

export function resolveUrl(url, def) {
  return transform(url) || transform(def);
}
