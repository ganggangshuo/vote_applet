export function getUID() {
  const uid = wx.getStorageSync("UID");

  return uid && JSON.parse(uid);
}

export function getToken() {
  return wx.getStorageSync("Token") || wx.getStorageSync("Token");
}

export function getAnonymousToken() {
  return wx.getStorageSync("AnonymousToken");
}

export function clearToken() {
  ["sid", "Token", "lastDate"].forEach(key => (
    wx.removeStorageSync(key)));
}

export function clearUID() {
  wx.removeStorageSync("UID");
}

export function clearAnonymousToken() {
  return wx.removeStorageSync("AnonymousToken");
}

export function refreshToken(isRemember, token) {
  token.AnonymousToken && 
    wx.setStorageSync("AnonymousToken", token.AnonymousToken),
    token.Token && 
    (wx.setStorageSync("lastDate", new Date().toDateString()), wx.setStorageSync("Token", token.Token)),
    wx.setStorageSync("sid", token.sid);
}

export function refreshUID(uid) {
  uid ? wx.setStorageSync("UID", JSON.stringify(uid)) : clearUID();
}

export function isAuth() {
  return !!(getToken() || getUID());
}

export function logout() {
  clearToken(), clearUID();
}