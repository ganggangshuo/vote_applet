
export function transform(promise, response) {
  return Object.assign(promise, {
    success: (fn) => {
      return promise.then(d => fn(d, response )), promise;
    },
    error: (fn) => {
      return promise.then(null, d => fn(d, response)), promise;
    },
    json: (fny) => {
      return typeof fn === "function" ? promise.then(fn) : promise.then(d => d.json());
    }
  });
}
