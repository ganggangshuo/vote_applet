import { Http } from './core.http'

export class HttpService {
  constructor(controllerName, http) {
    this.http = http;
    http ? (HttpService.http = http) : (this.http = HttpService.http || (HttpService.http = new Http()));
    this.controllerName = controllerName;
  }
  httpRequest(url, options) {
    return this.http.request(this.transformRequestUrl(url), options);
  }
  httpGet(url, options) {
    return this.http.get(this.transformUrl(url), options);
  }
  httpPost(url, body = null, options) {
    return this.http.post(this.transformUrl(url), body, options);
  }
  httpPut(url, body = null, options) {
    return this.http.put(this.transformUrl(url), body, options);
  }
  httpDelete(url, options) {
    return this.http.delete(this.transformUrl(url), options);
  }
  httpPatch(url, body = null, options) {
    return this.http.patch(this.transformUrl(url), body, options);
  }
  httpHead(url, options) {
    return this.http.head(this.transformUrl(url), options);
  }
  httpOptions(url, options) {
    return this.http.options(this.transformUrl(url), options);
  }
  httpUpload(url, data, options) {
    return this.httpPost(url, data, this.resolveRequestHeaders(new Headers(), options));
  }
  transformRequestUrl(url) {
    // if (url && url.url) {
    //   const request = this.transformUrl(url.url), params = Object.assign({}, url);
    //   delete params.url;
    //   return new (request, params);
    // }
    return this.transformUrl(url);
  }
  resolveRequestContentType(contentType = "application/json;charset=UTF-8", options) {
    return this.resolveRequestHeaders({ "Content-Type": contentType }, options);
  }
  resolveRequestHeaders(headers, options) {
    const rqheaders = headers instanceof Headers ? headers : new Headers(headers);
    return this.http.resolveRequestHeaders(rqheaders), options ? ((options.headers = rqheaders), options) : { headers: rqheaders };
  }
  resolveRequestParams(params, options) {
    return options ? ((options.params = params), options) : { params: params };
  }
  resolveUrl(...args) {
    return args.join("/");
  }
  transformUrl(url = "") {
    return this.controllerName + "/" + url;
  }
  getRootUrl(url = "") {
    return this.http.transformUrl(url);
  }
  get(id, options) {
    return this.httpGet(this.resolveUrl("Get", id), options);
  }
  getAll(data, options) {
    return this.httpGet(this.resolveUrl("GetAll"), this.resolveRequestParams(data, options));
  }
  getPaged(data, options) {
    return this.httpGet(this.resolveUrl("Get"), this.resolveRequestParams(data, options));
  }
  post(data, options) {
    return this.httpPost(this.resolveUrl("Post"), data, options);
  }
  put(id, data, options) {
    return this.httpPut(this.resolveUrl("Put", id), data, options);
  }
  delete(id, options) {
    return this.httpDelete(this.resolveUrl("Delete", id), options);
  }
  deleteAll(data, options) {
    return this.httpPut(this.resolveUrl("DeleteAll"), data, options);
  }
  getformValidInfo(mark, options) {
    return this.http.get(this.resolveUrl("App", "Form", "GetFormValidInfo"), options ? ((options.params = { mark: mark }), options) : { params: { mark: mark } });
  }
  valid(id, isValid, options) {
    return this.httpPut(this.resolveUrl("Valid", id), null, this.resolveRequestParams({ isValid: isValid }, this.resolveRequestContentType(undefined, options)));
  }
  sequence(data, options) {
    return this.httpPut(this.resolveUrl("Sequence"), data, options);
  }
  getCurrentUser(url = "GetCurrentUser", options) {
    return this.httpGet(this.resolveUrl(url), options);
  }
  enabled(id, options) {
    return this.httpPut(this.resolveUrl("Enabled", id), null, options);
  }
  disabled(id, options) {
    return this.httpPut(this.resolveUrl("Disabled", id), null, options);
  }
  publish(id, options) {
    return this.httpPut(this.resolveUrl("Publish", id), null, options);
  }
  enabledAll(id, options) {
    return this.httpPut(this.resolveUrl("EnabledAll"), id, options);
  }
  disabledAll(id, options) {
    return this.httpPut(this.resolveUrl("DisabledAll"), id, options);
  }
  publishAll(id, options) {
    return this.httpPut(this.resolveUrl("PublishAll"), id, options);
  }
}