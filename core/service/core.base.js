
function getHeaders() {
  return ({
    Accept: "application/json, text/plain, */*",
    "Content-Type": "application/json; charset=utf-8"
  });
}

export function getDefaultOptions(init) {
  return { headers: getHeaders(), ...init };
}

export function urlEncodeParams(params) {
  const searchParams =[];

  Object.keys(params).forEach(key => {
    const value = params[key];
    if (value && Array.isArray(value)) {
      value.forEach(element => {
        searchParams.push(key + '=' + encodeURI(element))
      });
    } else if (value) {
      searchParams.push(key + '=' + encodeURI(value))
    }
  });

  return searchParams.join('&');
}


export function fetch(input) {
  return new Promise((a, b) => {
    wx.request({
      url:input.url,
      method: input.method||'GET',
      header: input.header,
      data:input.body,
      success:function(d)  {
        if(d.statusCode>=200&&d.statusCode<300)
          a(d.data)
        else
          b(d.data);
      },fail:b
    })
  });
}